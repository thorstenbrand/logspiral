#    <logspiral, Blender addon for adding logarithmic spiral meshes.>
#    Copyright (C) <2021> <Thorsten Brand>
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name": "Add a Logarithmic spiral",
    "description": "Logarithmic spirals can be customised and added as mesh objects.",
    "author": "Thorsten Brand",
    "version": (1,0),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh",
    "category": "Object",
}

from .logspiral import *

logspiral
==
An addon to add custom logarithmic spirals as meshes into Blender.

Installation
--
Go to `Edit > Preferences > Addons > Install` and either select .zip file or the unzipped `logspiral.py` file.

Location
--
`View3D > Add > Mesh`

Features
--
* Adds a logarithmic spiral as a mesh to the blender scene.
* Number of vertices and geometric parameters can be customised.

Development
* When bumping versions increment both `bl_info` objects, one in `__init__.py` which is used for .zip install, and another in the main `uv_squares.py` file.

For any questions, bug reports or suggestions please contact me at **thorstenbrand@gmx.net**

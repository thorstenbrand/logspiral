#    <logspiral, Blender addon for adding logarithmic spiral meshes.>
#    Copyright (C) <2021> <Thorsten Brand>
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name": "Add a Logarithmic spiral",
    "description": "Logarithmic spirals can be customised and added as mesh objects.",
    "author": "Thorsten Brand",
    "version": (1,0),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh",
    "category": "Object",
}

import bpy
import numpy as np
import bmesh
from bpy_extras.object_utils import AddObjectHelper



def get_logspiral_vertices(a, b, c, alpha0, nturns, ndivisions):
    direction = np.sign(b)
    b = np.abs(b)
    nsteps = nturns * ndivisions
    alpha = np.linspace(0., nturns * 2.*np.pi, num = int(nsteps)) + alpha0/180.*np.pi
    x = a * np.cos(alpha) * np.exp(b*alpha)
    y = a * direction * np.sin(alpha) * np.exp(b*alpha)
    z = c * np.exp(b*alpha)
    return x, y, z


def add_logspiral(a, b, c, alpha0, nturns, ndivisions):

    x, y, z = get_logspiral_vertices(a, b, c, alpha0, nturns, ndivisions)
    verts = []
    edges = []
    for vertexID in range(0,x.size):
        xp = x[vertexID]
        yp = y[vertexID]
        zp = z[vertexID]
        vert = (xp,yp,zp)
        verts.append(vert)
        
    # fill edges
    for vertexID in range(0,x.size-1):
        edge = (vertexID, vertexID+1)
        edges.append(edge)
    return verts, edges




from bpy.props import (
    EnumProperty,
    FloatProperty,
    FloatVectorProperty
)


class AddLogspiral(bpy.types.Operator):
    """Add a logarithmic spiral mesh"""
    bl_idname = "mesh.primitive_logspiral_add"
    bl_label = "Log Spiral"
    bl_options = {'REGISTER', 'UNDO'}

    a: FloatProperty(
        name="starat radius",
        description="Start radius of spiral",
        min=0.0001, max=10000.0,
        default=0.1,
    )
    b: FloatProperty(
        name="opening angle",
        description="Opening angle of spiral",
        min=-10000.0, max=10000.0,
        default=0.1,
    )
    alpha0: FloatProperty(
        name="phase",
        description="Phase [deg]",
        min=0.0, max=360,
        default=0.,
    )
    nturns: FloatProperty(
        name="nturns",
        description="Number of turns",
        min=0.00001, max=np.inf,
        default=3.,
    )
    ndivisions: FloatProperty(
        name="ndivisions",
        description="Number of divisions per turn",
        min=4, max=1000000,
        default=64,
    )
    c: FloatProperty(
        name="zgrowth",
        description="Growth factor in third dimension",
        min=0, max=1000000,
        default=0,
    )

    # generic transform props
    align_items = (
        ('WORLD', "World", "Align the new object to the world"),
        ('VIEW', "View", "Align the new object to the view"),
        ('CURSOR', "3D Cursor", "Use the 3D cursor orientation for the new object")
    )
    align: EnumProperty(
        name="Align",
        items=align_items,
        default='WORLD',
        update=AddObjectHelper.align_update_callback,
    )
    location: FloatVectorProperty(
        name="Location",
        subtype='TRANSLATION',
    )
    rotation: FloatVectorProperty(
        name="Rotation",
        subtype='EULER',
    )

    def execute(self, context):

        verts_loc, edges = add_logspiral(
            self.a, 
			self.b, 
            self.c,
			self.alpha0, 
			self.nturns, 
			self.ndivisions
        )

        mesh = bpy.data.meshes.new("logspiral")

        bm = bmesh.new()

        for v_co in verts_loc:
            bm.verts.new(v_co)

        bm.verts.ensure_lookup_table()
        for e_idx in edges:
            bm.edges.new([bm.verts[i] for i in e_idx])

        bm.to_mesh(mesh)
        mesh.update()

        # add the mesh as an object into the scene with this utility module
        from bpy_extras import object_utils
        object_utils.object_data_add(context, mesh, operator=self)

        return {'FINISHED'}


def menu_func(self, context):
    self.layout.operator(AddLogspiral.bl_idname, icon='MESH_CUBE')


def register():
    bpy.utils.register_class(AddLogspiral)
    bpy.types.VIEW3D_MT_mesh_add.append(menu_func)


def unregister():
    bpy.utils.unregister_class(AddLogspiral)
    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func)


if __name__ == "__main__":
    register()
